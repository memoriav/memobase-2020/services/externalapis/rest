package modules

import co.elastic.clients.elasticsearch.ElasticsearchClient

trait ElasticsearchComponent {
  val client: ElasticsearchClient
}
