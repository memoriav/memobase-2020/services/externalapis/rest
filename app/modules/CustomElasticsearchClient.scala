package modules

import co.elastic.clients.elasticsearch.ElasticsearchClient
import co.elastic.clients.elasticsearch._types.StoredScript
import co.elastic.clients.elasticsearch.core.PutScriptRequest

import java.io.{File, FileInputStream, IOException}
import com.typesafe.config.Config

import javax.inject.{Inject, Singleton}
import org.apache.http.message.BasicHeader
import org.apache.http.{Header, HttpHost}
import play.Environment
import play.api.inject.ApplicationLifecycle
import utilities.FileUtil

import scala.concurrent.Future
import co.elastic.clients.transport.rest_client.RestClientTransport
import co.elastic.clients.json.jackson.JacksonJsonpMapper
import modules.CustomElasticsearchClient.{buildSSLContext, parseHost}
import org.apache.http.ssl.SSLContexts
import org.elasticsearch.client.RestClient

import java.nio.charset.StandardCharsets
import java.security.KeyStore
import java.security.cert.CertificateFactory
import java.util.Base64
import javax.net.ssl.SSLContext
import scala.util.Try

@Singleton
class CustomElasticsearchClient @Inject() (
    lifecycle: ApplicationLifecycle,
    private val config: Config,
    private val env: Environment
) extends ElasticsearchComponent {
  lifecycle.addStopHook(() => {
    Future.successful(client.shutdown())
  })

  val client: ElasticsearchClient = connect()

  uploadTemplates()

  private def connect(): ElasticsearchClient = {
    val hosts = parseHost(config.getString("index.hosts")).get
    val apiKeyAuth = Base64.getEncoder.encodeToString(
      s"${config.getString("index.apiKeyId")}:${config.getString("index.apiKeySecret")}"
        .getBytes(StandardCharsets.UTF_8)
    )
    val headers = Array(
      new BasicHeader("cluster.name", config.getString("index.cluster"))
        .asInstanceOf[Header],
      new BasicHeader("Authorization", s"ApiKey $apiKeyAuth")
        .asInstanceOf[Header]
    )

    val sslContext = buildSSLContext(config.getString("index.caCertPath")).get
    val restClient =
      RestClient
        .builder(hosts: _*)
        .setHttpClientConfigCallback(httpClientBuilder =>
          httpClientBuilder.setSSLContext(sslContext)
        )
        .setDefaultHeaders(headers)
        .build()

    val transport = new RestClientTransport(
      restClient,
      new JacksonJsonpMapper()
    )
    new ElasticsearchClient(transport)
  }

  private def uploadTemplates(): Unit = {
    config
      .getStringList("index.templatequeries")
      .forEach((templateName: String) => {
        val template = FileUtil.readFile(templateName, env)
        val nameWithoutPath = templateName
          .substring(templateName.lastIndexOf("/") + 1)
          .replaceAll(".mustache", "")
        val templatePrefix = config.getString("index.template_prefix")
        try {
          client.putScript((req: PutScriptRequest.Builder) =>
            req
              .id(s"${templatePrefix}_$nameWithoutPath")
              .script((s: StoredScript.Builder) =>
                s
                  .lang("mustache")
                  .source(template.replace("\n", ""))
              )
          )
        } catch {
          case io: IOException =>
            // TODO: if the templates cannot be sent the application should shut down.
            io.printStackTrace()
        }
      })
  }
}

object CustomElasticsearchClient {
  private val hostRegex = """^(https?)://(.*):(\d+)$""".r
  def parseHost(hosts: String): Try[Array[HttpHost]] = Try {
    hosts
      .split(',')
      .map(h => {
        val hostParts = hostRegex.findFirstMatchIn(h).get
        val scheme = hostParts.group(1)
        val hostName = hostParts.group(2)
        val port = hostParts.group(3).toInt
        new HttpHost(hostName, port, scheme)
      })
  }

  private def buildSSLContext(caCertPath: String): Try[SSLContext] = {
    Try {
      val factory = CertificateFactory.getInstance("X.509")
      val trustedCa =
        factory.generateCertificate(new FileInputStream(new File(caCertPath)))
      val trustStore = KeyStore.getInstance("pkcs12")
      trustStore.load(null, null)
      trustStore.setCertificateEntry("ca", trustedCa)
      SSLContexts.custom.loadTrustMaterial(trustStore, null).build
    }
  }
}
