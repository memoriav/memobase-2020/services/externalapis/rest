package modules

import play.api.{Configuration, Logging}
import play.api.libs.json.{JsValue, Json}
import utilities.{PropertyHelper, SearchHelper}

import javax.inject.{Inject, Singleton}
import scala.util.{Failure, Success}

@Singleton
class ElasticsearchProperties @Inject() (
    es: ElasticsearchComponent,
    private val config: Configuration
) extends Logging {
  private val searchHelper = new SearchHelper(es, config)
  val esProperties = Map(
    "record" -> handleRequest("record"),
    "recordSet" -> handleRequest("recordSet"),
    "institution" -> handleRequest("institution")
  )

  private def handleRequest(concept: String): String = {
    PropertyHelper.getPropertyMap(concept, config, es) match {
      case Success(propertyMap) => Json.stringify(propertyMap)
      case Failure(ex) =>
        throw new Error(s"Errors while building index property maps: $ex")
    }
  }

}
