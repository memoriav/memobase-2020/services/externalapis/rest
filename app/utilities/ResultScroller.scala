package utilities

import org.apache.pekko.stream.scaladsl
import org.apache.pekko.util.ByteString
import co.elastic.clients.elasticsearch._types.Time
import co.elastic.clients.elasticsearch.core.search.Hit
import co.elastic.clients.elasticsearch.core.{
  ClearScrollRequest,
  ScrollRequest,
  ScrollResponse,
  SearchResponse,
  SearchTemplateRequest,
  SearchTemplateResponse
}
import libraries.JsonLines
import modules.ElasticsearchComponent
import com.fasterxml.jackson.databind.node.ObjectNode

import java.util
import play.api.{Configuration, Logging}
import play.api.mvc._
import play.api.mvc.Results.Ok

import scala.util.{Failure, Success, Try}

class ResultScroller(
    es: ElasticsearchComponent,
    config: Configuration,
    limit: Int
) extends Logging {

  private def initiateScroll(
      q: String,
      exists: String,
      concept: String,
      terms: Map[String, List[String]] = Map()
  ): SearchTemplateResponse[ObjectNode] = {
    val searchContext = if (terms.nonEmpty) {
      TermsContext(terms)
    } else {
      SearchQueryContext(q, exists)
    }
    val templateSearchRequest: SearchTemplateRequest =
      new SearchTemplateRequestBuilder(config, concept, 200)
        .setSearchContext(searchContext)
        .setFetchContext(Scroll)
        .build
    es.client.searchTemplate(templateSearchRequest, classOf[ObjectNode])
  }

  private def executeStreamResponse(
      hitsIterator: util.Iterator[Hit[
        com.fasterxml.jackson.databind.node.ObjectNode
      ]],
      scrollId: String,
      concept: String
  ): Result = {
    val source = limit match {
      case l if l > 0 =>
        scaladsl.Source
          .fromIterator(() => scrollIterator(hitsIterator, scrollId))
          .take(l)
      case _ =>
        scaladsl.Source.fromIterator(() =>
          scrollIterator(hitsIterator, scrollId)
        )
    }
    Ok.chunked(source)
      .withHeaders(
        (
          "Content-Disposition",
          s"attachment; filename=memobase-$concept-bulk.jsonl"
        )
      )
      .as(JsonLines.mimeTypes.head)
  }

  private def scrollIterator(
      _hitsIterator: util.Iterator[Hit[
        com.fasterxml.jackson.databind.node.ObjectNode
      ]],
      _scrollId: String
  ): Iterator[ByteString] = {
    new Iterator[ByteString]() {
      private var hitsIterator
          : util.Iterator[Hit[com.fasterxml.jackson.databind.node.ObjectNode]] =
        _hitsIterator
      private var scrollId: String = _scrollId
      override def hasNext: Boolean = {
        if (!hitsIterator.hasNext) {
          scrollRequest(scrollId) match {
            case Success(response) =>
              hitsIterator = response.hits().hits().iterator()
              scrollId = response.scrollId()
              if (!hitsIterator.hasNext) {
                val clearScrollRequest =
                  new ClearScrollRequest.Builder().scrollId(scrollId).build()
                es.client.clearScroll(clearScrollRequest)
                false
              } else {
                true
              }
            case Failure(_) => false
          }
        } else {
          true
        }
      }

      override def next(): ByteString = {
        ByteString.fromString(hitsIterator.next().source().toString + "\n")
      }
    }
  }

  private def scrollRequest(
      scrollId: String
  ): Try[ScrollResponse[ObjectNode]] = {
    val request = new ScrollRequest.Builder()
      .scrollId(scrollId)
      .scroll(Time.of(t => t.time("10m")))
      .build()
    Try {
      es.client.scroll(request, classOf[ObjectNode])
    }
  }

}

object ResultScroller {
  def scrollThroughResultList(
      q: String,
      exists: String,
      es: ElasticsearchComponent,
      config: Configuration,
      limit: Int
  ): Result = {
    val resultScroller = new ResultScroller(es, config, limit)
    val response = resultScroller.initiateScroll(q, exists, "record")
    resultScroller.executeStreamResponse(
      response.hits.hits.iterator,
      response.scrollId,
      "record"
    )
  }

  def scrollThroughAdvancedSearchResultList(
      response: SearchResponse[com.fasterxml.jackson.databind.node.ObjectNode],
      es: ElasticsearchComponent,
      config: Configuration,
      limit: Int
  ): Result = {
    val resultScroller = new ResultScroller(es, config, limit)
    resultScroller.executeStreamResponse(
      response.hits.hits.iterator,
      response.scrollId,
      "record"
    )
  }

  def scrollThroughBasket(
      q: String,
      es: ElasticsearchComponent,
      config: Configuration,
      limit: Int
  ): Result = {
    val ids = UriDecompressor
      .decompress(q)
      .split("&")
      .map(id => s"https://memobase.ch/record/$id")
      .toList
    val resultScroller = new ResultScroller(es, config, limit)
    val response =
      resultScroller.initiateScroll("", "", "record", Map("_id" -> ids))
    resultScroller.executeStreamResponse(
      response.hits.hits.iterator,
      response.scrollId,
      "record"
    )
  }
}
