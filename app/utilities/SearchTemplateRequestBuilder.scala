package utilities

import co.elastic.clients.elasticsearch._types.Time
import co.elastic.clients.elasticsearch.core.SearchTemplateRequest
import co.elastic.clients.json.JsonData
import play.api.Configuration

import scala.jdk.CollectionConverters._

class SearchTemplateRequestBuilder(
    configuration: Configuration,
    concept: String,
    size: Int
) {
  private val searchTemplateRequestBuilder = new SearchTemplateRequest.Builder()
  private var fetchContext: FetchContext = _
  private val blacklist: Map[String, Seq[String]] =
    if (configuration.has("blacklist")) {
      configuration.get[Map[String, Seq[String]]]("blacklist")
    } else {
      Map()
    }
  private val showPublishedDocumentsOnly: Boolean =
    ConfigUtil.showPublishedDocumentsOnly(configuration)
  private val hideInventoryInstitutions: Boolean =
    ConfigUtil.hideInventoryInstitutions(configuration)
  private val showPublishedInventoryInstitutionsOnly: Boolean =
    ConfigUtil.showPublishedInventoryInstitutionsOnly(configuration)
  private val scriptParamBuilder: ScriptParamBuilder = new ScriptParamBuilder()
    .setSize(configuration, size)
    .setBlacklistedDocuments(
      concept,
      blacklist,
      showPublishedDocumentsOnly,
      hideInventoryInstitutions,
      showPublishedInventoryInstitutionsOnly
    )

  def build: SearchTemplateRequest = {
    searchTemplateRequestBuilder.index(
      configuration.get[String]("concept2index." + concept)
    )
    fetchContext match {
      case Scroll =>
        searchTemplateRequestBuilder.scroll(Time.of(t => t.time("1m")))
      case From(o) => scriptParamBuilder.setOffset(o)
    }
    searchTemplateRequestBuilder.params(
      scriptParamBuilder.map.map(e => (e._1, JsonData.of(e._2))).asJava
    ) // (scriptParamBuilder.map)
    searchTemplateRequestBuilder.build
  }

  def setFetchContext(context: FetchContext): SearchTemplateRequestBuilder = {
    this.fetchContext = context
    this
  }

  def setSearchContext(context: SearchContext): SearchTemplateRequestBuilder = {
    context match {
      case TermsContext(terms) =>
        searchTemplateRequestBuilder.id(
          generateTemplateName("termsFilterQuery")
        )
        scriptParamBuilder.setTermsFilter(terms)
      case SearchQueryContext(q, e) if e.nonEmpty =>
        searchTemplateRequestBuilder.id(
          generateTemplateName("existsFilterQuery")
        )
        scriptParamBuilder.setExistsFilterQuery(q, e)
      case SearchQueryContext("", _) =>
        searchTemplateRequestBuilder.id(generateTemplateName("matchAllQuery"))
      case SearchQueryContext(q, _) =>
        searchTemplateRequestBuilder.id(generateTemplateName("multiMatchQuery"))
        scriptParamBuilder.setSimpleParamQuery(q)
    }
    this
  }

  private def generateTemplateName(templateKey: String): String = {
    val templatePrefix: String =
      configuration.get[String]("index.template_prefix")
    val templateName =
      configuration.get[String]("query2template." + templateKey)
    s"${templatePrefix}_$templateName"
  }
}
