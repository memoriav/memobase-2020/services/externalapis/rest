package utilities

import play.api.libs.json._

object JsonUtil {

  def filterAndSortJson(
      js: JsValue,
      documentId: String,
      fieldExclusionTableForConcept: Map[String, Seq[String]]
  ): JsValue =
    filterByPath(js, filteredFields(documentId, fieldExclusionTableForConcept))

  def hideUiElements(
      documentId: String,
      fieldExclusionTableForConcept: Map[String, Seq[String]]
  ): String = {
    val filtered =
      filteredFields(documentId, fieldExclusionTableForConcept)
        .filter(_.startsWith("_"))
        .map(_.substring(1))
    Json.toJson(filtered).toString
  }

  private def filteredFields(
      documentId: String,
      fieldExclusionTableForConcept: Map[String, Seq[String]]
  ): Set[String] = (for {
    filterKeys <- fieldExclusionTableForConcept.keys
    filterKey <- filterKeys.split(',').toSeq
    filterKeyLen = filterKey.length
    if filterKey == "_" || (filterKeyLen <= documentId.length && documentId
      .substring(
        0,
        filterKeyLen
      ) == filterKey)
  } yield fieldExclusionTableForConcept(filterKeys)).flatten.toSet

  private def filterByPath(
      js: JsValue,
      filteredPaths: Set[String],
      currentPath: List[String] = List()
  ): JsValue = js match {
    case JsObject(fields) =>
      JsObject(
        fields.toSeq
          .filter(v =>
            !filteredPaths.contains((currentPath :+ v._1).mkString("."))
          )
          .sortBy(_._1)
          .map { case (key, value) =>
            (
              key,
              filterByPath(
                value.asInstanceOf[JsValue],
                filteredPaths,
                currentPath :+ key
              )
            )
          }
      )
    case JsArray(elements) =>
      JsArray(elements.map(e => filterByPath(e, filteredPaths, currentPath)))
    case _ => js

  }
}
