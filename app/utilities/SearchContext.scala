package utilities

sealed trait SearchContext

case class TermsContext(terms: Map[String, List[String]]) extends SearchContext
case class SearchQueryContext(q: String, exists: String) extends SearchContext
