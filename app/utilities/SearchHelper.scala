package utilities

// import com.github.diogoduailibe.lzstring4j.LZString

import co.elastic.clients.elasticsearch.core.search.SourceConfigParam
import co.elastic.clients.elasticsearch.core.{
  GetRequest,
  GetResponse,
  SearchTemplateRequest,
  SearchTemplateResponse
}
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import libraries.Format
import modules.ElasticsearchComponent
import play.api.Configuration
import play.api.libs.json._
import play.api.libs.json.jackson.PlayJsonMapperModule

import scala.jdk.CollectionConverters._
import scala.jdk.javaapi.CollectionConverters
import scala.util.Try

class SearchHelper(
    val es: ElasticsearchComponent,
    val configuration: Configuration
) extends CheckedPaging {

  val mapper: ObjectMapper =
    new ObjectMapper().registerModule(
      new PlayJsonMapperModule(JsonParserSettings())
    )

  def search(
      q: String,
      exists: String,
      format: Format,
      offset: Long,
      size: Int,
      concept: ConceptType
  ): JsValue = {
    doChecks(offset, size, configuration.get[Int]("index.maxsize"))

    val request: SearchTemplateRequest =
      new SearchTemplateRequestBuilder(configuration, concept, size)
        .setFetchContext(From(offset))
        .setSearchContext(SearchQueryContext(q, exists))
        .build
    val createPagingView = (totalHits: Long) =>
      new PagingMetadataHelper(
        q,
        format,
        offset,
        size,
        totalHits,
        configuration
      ).searchPaging(exists, concept)
    pagedSearch(request, createPagingView)
  }

  def searchById(id: String, concept: ConceptType): JsValue = {
    val fullId =
      if (concept == Record || concept == Institution)
        s"https://memobase.ch/$concept/$id"
      else id
    idIsBlacklisted(id).get
    val excludes =
      (if (configuration.has(s"ignoredFields.${concept}s._")) {
         configuration.get[Seq[String]](s"ignoredFields.${concept}s._")
       } else {
         Seq()
       }).asJava
    val getRequest = new GetRequest.Builder()
      .index(configuration.get[String](s"concept2index.$concept"))
      .id(fullId)
      .source(new SourceConfigParam.Builder().fetch(true).build())
      .sourceExcludes(excludes)
      .build()
    val getResponse: GetResponse[ObjectNode] =
      es.client.get(getRequest, classOf[ObjectNode])
    if (getResponse.found) {
      if (blacklistedByAttribute(getResponse.source, concept)) {
        throw new Exception(s"Access to $fullId is restricted")
      }
      toJsObject(getResponse.source)
    } else {
      throw new Exception(s"$concept with id $fullId does not exist")
    }
  }

  private def blacklistedByAttribute(
      response: ObjectNode,
      concept: ConceptType
  ): Boolean = {
    concept match {
      case Record =>
        ConfigUtil.showPublishedDocumentsOnly(configuration) && Try(
          !response
            .get("isPublished")
            .asBoolean
        ).getOrElse(false)
      case Institution =>
        (ConfigUtil
          .hideInventoryInstitutions(configuration) && Try(
          response
            .get("type")
            .asText == "memobaseInstitutionInventoryProject"
        ).getOrElse(false)) ||
        // TODO: At the moment, the flag in the index is inverse. Change the following line after fix applied in index
        (ConfigUtil.showPublishedInventoryInstitutionsOnly(
          configuration
        ) && Try(
          response
            .get("isOrWasHolderOf")
            .get("publicationPermitted")
            .asBoolean
        ).getOrElse(false))
      case _ => false
    }
  }

  def getRecordSetAndInstitutionIdFromResult(
      result: JsValue
  ): (Option[String], Option[String]) = {
    result match {
      case o: JsObject =>
        (
          jsValueToString(
            o.value.get("isOrWasPartOf"),
            x => Some(x.value.replace("mbrs:", ""))
          ),
          jsValueToString(
            o.value.get("hasOrHadHolder.@id"),
            x => Some(x.value.replace("mbcb:", ""))
          )
        )
      case _ => (None, None)
    }
  }

  def getInstitutionNameById(id: String): String = {
    searchById(id, Institution) match {
      case o: JsObject =>
        Set(
          jsValueToString(o.value.get("name_de")),
          jsValueToString(o.value.get("name_fr")),
          jsValueToString(o.value.get("name_it"))
        ).flatten
          .mkString(" - ")
      case _ => ""
    }
  }

  def getRecordSetNameById(id: String): String = {
    searchById(id, RecordSet) match {
      case o: JsObject =>
        Set(
          jsValueToString(o.value.get("title_de")),
          jsValueToString(o.value.get("title_fr")),
          jsValueToString(o.value.get("title_it"))
        ).flatten
          .mkString(" - ")
      case _ => ""
    }
  }

  private def jsValueToString(
      value: Option[JsValue],
      replacementFn: JsString => Option[String] = x => Some(x.value)
  ): Option[String] = {
    value.flatMap {
      case x: JsString => replacementFn(x)
      case _           => None
    }
  }

  def searchByIds(
      q: String,
      format: Format,
      offset: Long,
      size: Int
  ): JsValue = {
    val ids = UriDecompressor
      .decompress(q)
      .split("&")
      .map(id => s"https://memobase.ch/record/$id")
      .toList
    val request =
      new SearchTemplateRequestBuilder(configuration, "record", size)
        .setFetchContext(From(offset))
        .setSearchContext(TermsContext(Map("_id" -> ids)))
        .build
    val createPagingHelper = (totalHits: Long) =>
      new PagingMetadataHelper(
        q,
        format,
        offset,
        size,
        totalHits,
        configuration
      ).getIds
    pagedSearch(request, createPagingHelper)
  }

  private def pagedSearch(
      request: SearchTemplateRequest,
      createPagingView: Long => JsValue,
  ): JsValue = {
    val searchTemplateResponse: SearchTemplateResponse[ObjectNode] =
      es.client.searchTemplate(request, classOf[ObjectNode])
    val totalHits = searchTemplateResponse.hits.total.value
    val pagingView = createPagingView(totalHits)
    val hits = CollectionConverters
      .asScala(searchTemplateResponse.hits.hits.iterator)
      .map(v => mapper.treeToValue(v.source, classOf[JsObject]))
      .toIndexedSeq
    Json.obj(
      "@id" -> pagingView("@id")
        .as[String]
        .replaceAll("&?offset=[^&^]+", "")
        .replaceAll("&?size=[^&^]+", ""),
      "@type" -> "hydra:Collection",
      "@context" -> Json.obj(
        "hydra" -> "http://www.w3.org/ns/hydra/core#"
      ),
      "hydra:member" -> new JsArray(hits),
      "hydra:totalItems" -> totalHits,
      "hydra:view" -> pagingView
    )
  }

  private def toJsObject(response: ObjectNode): JsObject = {
    mapper.treeToValue(response, classOf[JsObject])
  }

  private def idIsBlacklisted(id: String): scala.util.Try[Unit] = {
    def objectIsBlacklisted(
        pathToBlacklist: String,
        id: String,
        splitIdAt: Int
    ): Boolean = {
      val blacklist = configuration
        .getOptional[Seq[String]](pathToBlacklist)
        .getOrElse(List())
      val normalisedId = id.split('-').take(splitIdAt).mkString("-")
      blacklist.contains(normalisedId)
    }

    lazy val blacklistedRecord: String => Boolean = id =>
      configuration
        .getOptional[Seq[String]]("blacklist.records")
        .getOrElse(List())
        .contains(id)
    lazy val blacklistedRecordSet: String => Boolean = id =>
      objectIsBlacklisted("blacklist.recordSets", id, 2)
    lazy val blacklistedInstitution: String => Boolean = id =>
      objectIsBlacklisted("blacklist.institutions", id, 1)
    if (blacklistedRecord(id)) {
      scala.util.Failure(
        RestrictedResourceException(s"access to document $id is restricted")
      )
    } else if (blacklistedRecordSet(id)) {
      scala.util.Failure(
        RestrictedResourceException(
          s"access to record set ${id.split('-').take(2).mkString("-")} is restricted"
        )
      )
    } else if (blacklistedInstitution(id)) {
      scala.util.Failure(
        RestrictedResourceException(
          s"access to institution ${id.split('-')(0)} is restricted"
        )
      )
    } else {
      scala.util.Success(())
    }
  }
}
