package utilities

import co.elastic.clients.elasticsearch.ElasticsearchClient
import co.elastic.clients.elasticsearch._types.query_dsl.{
  Operator,
  QueryStringQuery
}
import co.elastic.clients.elasticsearch._types.Time
import co.elastic.clients.elasticsearch.core.search.TrackHits
import co.elastic.clients.elasticsearch.core.{SearchRequest, SearchResponse}
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import play.api.Configuration
import play.api.libs.json.JsonParserSettings
import play.api.libs.json.jackson.PlayJsonMapperModule

import scala.collection.mutable.ListBuffer

class AdvancedSearchHelper(
    private val query: String,
    private val offset: Long,
    private val resultListSize: Int,
    private val concept: ConceptType,
    private val configuration: Configuration,
    private val elastic: ElasticsearchClient
) extends CheckedPaging {
  doChecks(offset, resultListSize, configuration.get[Int]("index.maxsize"))

  private def cleanQuery(): String = {
    var resultQuery = query
    val namespaces = configuration.get[Map[String, String]]("namespaces").keySet
    for (ns <- namespaces) {
      resultQuery = resultQuery.replace(ns + ":", ns + "\\:")
    }
    resultQuery = resultQuery.replace("http:", "http\\:")
    resultQuery = resultQuery.replace("https:", "https\\:")
    if (resultQuery.count(c => c == '/') != 2) {
      resultQuery = resultQuery.replace("/", "\\/")
    }
    resultQuery + generateBlacklistQuery
  }

  private def generateBlacklistQuery: String = {
    val query = ListBuffer[String]()
    val showPublishedDocumentsOnly =
      ConfigUtil.showPublishedDocumentsOnly(configuration)
    if (showPublishedDocumentsOnly) {
      query += "NOT isPublished:false"
    }

    val hideInventoryInstitution = concept == Institution &&
      ConfigUtil.hideInventoryInstitutions(configuration)
    if (hideInventoryInstitution) {
      query += "NOT type:memobaseInstitutionInventoryProject"
    }

    val showPublishedInventoryInstitutionsOnly = concept == Institution && ConfigUtil.showPublishedInventoryInstitutionsOnly(configuration)
    // TODO: At the moment, the flag in the index is inverse. Change the following line after fix applied in index
    if (showPublishedInventoryInstitutionsOnly) {
      query += "isOrWasHolderOf.publicationPermitted:false"
    }


    val blacklistQuery = (if (configuration.has("blacklist")) {
                            val blacklisted =
                              configuration.get[Map[String, Seq[String]]](
                                "blacklist"
                              )
                            val recordSets: String => String = prefix =>
                              blacklisted
                                .getOrElse("recordSets", List())
                                .map(r => s"$prefix$r")
                                .mkString(" OR ")
                            val institutions: String => String = prefix =>
                              blacklisted
                                .getOrElse("institutions", List())
                                .map(i => s"$prefix$i")
                                .mkString(" OR ")
                            concept match {
                              case Institution =>
                                if (institutions("").nonEmpty) {
                                  s"NOT _id:(${institutions("https\\:\\/\\/memobase.ch\\/institution\\/")})"
                                } else ""
                              case RecordSet =>
                                List(
                                  (if (recordSets("").nonEmpty) {
                                     Some(s"NOT _id:(${recordSets("")})")
                                   } else None),
                                  (if (institutions("").nonEmpty) {
                                     // in record-sets index, this field doesn't have the subfield @id and values miss the mbcb: prefix
                                     Some(s"NOT hasOrHadHolder:(${institutions("")})")
                                   } else None)
                                ).flatten.mkString(" AND ")
                              case Record =>
                                val records: String = blacklisted
                                  .getOrElse("records", List())
                                  .map(r =>
                                    s"https\\:\\/\\/memobase.ch\\/record\\/$r"
                                  )
                                  .mkString("+OR+")
                                List(
                                  (if (records.nonEmpty) {
                                     Some(s"NOT _id:($records)")
                                   } else None),
                                  (if (recordSets("").nonEmpty) {
                                     Some(
                                       s"NOT isOrWasPartOf.keyword:(${recordSets("mbrs\\:")})"
                                     )
                                   } else None),
                                  (if (institutions("").nonEmpty) {
                                     Some(
                                       s"NOT hasOrHadHolder.@id:(${institutions("mbcb\\:")})"
                                     )
                                   } else None)
                                ).flatten.mkString(" AND ")
                              case UnknownConcept => ""
                            }
                          } else {
                            ""
                          })
    if (blacklistQuery.nonEmpty) {
      query += blacklistQuery
    }
    if (query.nonEmpty) {
      s" AND ${query.mkString(" AND ")}"
    } else {
      ""
    }
  }

  def execute(scroll: Boolean = false): SearchResponse[ObjectNode] = {
    val request = buildRequest(scroll)
    elastic.search(request, classOf[ObjectNode])
  }

  private def buildRequest(scroll: Boolean): SearchRequest = {
    val query = new QueryStringQuery.Builder()
      .query(cleanQuery())
      .defaultOperator(Operator.And)
      .escape(false)
      .build()
      ._toQuery()

    // TODO: Define defaultFields for concepts
    /* concept match {
      case utilities.ConceptType.bibliographicResource =>
        queryBuilder.defaultField("dct:title")
      case utilities.ConceptType.work =>
      case utilities.ConceptType.person =>
        queryBuilder.defaultField("rdfs:label")
        searchSourceBuilder.sort("dbo:thumbnail", SortOrder.DESC)
      case utilities.ConceptType.organisation =>
        queryBuilder.defaultField("rdfs:label")
        searchSourceBuilder.sort("dbo:thumbnail", SortOrder.DESC)
      case utilities.ConceptType.item =>
        queryBuilder.defaultField("bibo:locator")
      case utilities.ConceptType.document =>
        queryBuilder.defaultField("bf:local")
    }*/

    val searchRequestBuilder = new SearchRequest.Builder()
      .index(index)
      .query(query)
      .size(resultListSize)
      .from(if (offset > Int.MaxValue) Int.MaxValue else offset.toInt)
      .trackTotalHits(TrackHits.of(thb => thb.enabled(true)))

    if (scroll) searchRequestBuilder.scroll(Time.of(t => t.time("5m")))
    searchRequestBuilder.build()
  }

  private def index: String =
    configuration.get[String]("concept2index." + concept)
}
