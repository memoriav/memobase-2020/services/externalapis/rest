package utilities

trait CheckedPaging {
  def doChecks(offset: Long, resultListSize: Int, maxIndexSize: Int): Unit = {
    if (offset < 0) {
      throw new Exception("Offset number should be positive")
    } else if (resultListSize < 1) {
      throw new Exception("Number of results requested should be positive")
    } else if (resultListSize > maxIndexSize) {
      throw new Exception(
        s"Cannot request more than $maxIndexSize results in one request. But you can get dumps. See documentation."
      )
    } else if (offset > 10000) {
      throw new Exception("Cannot go further than the 10000th result")
    }
  }
}
