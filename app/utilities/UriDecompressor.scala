package utilities

import scala.collection.mutable
import scala.language.{implicitConversions, postfixOps}
import Utils._

import scala.annotation.tailrec

object UriDecompressor {
  private val keyStrUriSafe: String =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-$"
  private val baseReverseDic: Map[Char, Int] =
    keyStrUriSafe.toCharArray.zipWithIndex
      .foldRight(Map[Char, Int]())((x, agg) => agg + (x._1 -> x._2))

  private val getBaseValue: Char => Option[Char] = c =>
    baseReverseDic.get(c).map(_.toChar)

  def decompress(input: String): String = {
    if (input.isEmpty) {
      ""
    } else {
      val urlEncodedInputStr = input.replaceAll(" ", "+")
      decompress(
        urlEncodedInputStr.length,
        32,
        i => getBaseValue(urlEncodedInputStr.charAt(i))
      )
    }
  }

  private def decompress(
      inputLength: Int,
      resetValue: Int,
      nextValue: Int => Option[Char]
  ): String = {
    val result = new StringBuilder

    val data = Data(resetValue, nextValue)
    val bits = data.compute(2.asPowerOf2)
    val c = bits match {
      case 0 => data.compute(8.asPowerOf2).stringifyCharValue
      case 1 => data.compute(16.asPowerOf2).stringifyCharValue
      case 2 => ""
    }
    val dictionary: mutable.Buffer[String] = mutable.Buffer("0", "1", "2", c)
    result.append(c)

    iterate(data, inputLength, 3, dictionary, result, c, 4)

  }

  @tailrec
  private def iterate(
      data: Data,
      inputLength: Int,
      numBitsParam: Int,
      dictionary: mutable.Buffer[String],
      result: StringBuilder,
      w: String,
      enlargeInParam: Int
  ): String = {
    if (data.index > inputLength) {
      ""
    } else {
      var numBits = numBitsParam
      val bits = data.compute(numBits.asPowerOf2)
      var (c, enlargeIn) = bits match {
        case 0 =>
          dictionary += data.compute(8.asPowerOf2).stringifyCharValue
          (dictionary.size - 1, enlargeInParam - 1)
        case 1 =>
          dictionary += data.compute(16.asPowerOf2).stringifyCharValue
          (dictionary.size - 1, enlargeInParam - 1)
        case 2 =>
          return result.toString
        case _ =>
          (bits, enlargeInParam)
      }

      if (enlargeIn == 0) {
        enlargeIn = numBits.asPowerOf2
        numBits = numBits + 1
      }

      val entry = if (c < dictionary.size && dictionary(c).nonEmpty) {
        dictionary(c)
      } else {
        if (c == dictionary.size) {
          w + w.charAt(0)
        } else {
          return ""
        }
      }
      result.append(entry)
      dictionary += (w + entry.charAt(0))

      enlargeIn = enlargeIn - 1
      if (enlargeIn == 0) {
        enlargeIn = numBits.asPowerOf2
        numBits = numBits + 1
      }

      iterate(data, inputLength, numBits, dictionary, result, entry, enlargeIn)
    }
  }

}

class Data(
    var value: Char,
    var position: Int,
    var index: Int,
    resetValue: Int,
    nextValue: Int => Option[Char]
) {

  @tailrec
  private def compute(
      value: Char,
      position: Int,
      index: Int,
      bits: Int,
      power: Int,
      maxPower: Int
  ): Int = {
    if (power == maxPower) {
      this.value = value
      this.position = position
      this.index = index
      bits
    } else {
      val resb = value & position
      val newPosition = position >> 1
      val newBits = bits | ((if (resb > 0) 1 else 0) * power)
      if (newPosition == 0) {
        compute(
          this.nextValue(index).get,
          this.resetValue,
          index + 1,
          newBits,
          power << 1,
          maxPower
        )
      } else {
        compute(value, newPosition, index, newBits, power << 1, maxPower)
      }
    }
  }

  def compute(maxPower: Int): Int = {
    compute(this.value, this.position, this.index, 0, 1, maxPower)
  }
}

object Data {
  def apply(resetValue: Int, nextValue: Int => Option[Char]): Data = {
    new Data(nextValue(0).get, resetValue, 1, resetValue, nextValue)
  }
}

protected object Utils {
  implicit class ExtendedInt(val num: Int) {
    def asPowerOf2: Int = {
      1 << num
    }

    def stringifyCharValue: String = num.toChar.toString
  }
}
