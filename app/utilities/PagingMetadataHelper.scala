package utilities

import libraries.Format
import play.api.Configuration
import play.api.libs.json.{JsValue, Json}

class PagingMetadataHelper(
    q: String,
    format: Format,
    offsetParam: Long,
    sizeParam: Int,
    totalItems: Long,
    configuration: Configuration
) {

  private val size = sizeParam
  private val offset =
    if (offsetParam >= totalItems) totalItems - 1 else offsetParam
  private val lastOffset = offset + ((totalItems - offset) / size).toInt * size

  private val baseUrl = configuration.get[String]("baseUrl")

  private def searchPagingBuilder(routeBuilder: Long => String): JsValue = {
    var value = Json.obj(
      "@id" -> routeBuilder(offset),
      "hydra:first" -> routeBuilder(0),
      "hydra:last" -> routeBuilder(lastOffset),
      if (totalItems - offset < size) "hydra:limit" -> (totalItems - offset)
      else "hydra:limit" -> size
    )
    if (offset - size >= 0)
      value = value ++ Json.obj("hydra:previous" -> routeBuilder(offset - size))
    else if (offset > 0)
      value = value ++ Json.obj("hydra:previous" -> routeBuilder(0))
    if ((offset + size) <= lastOffset)
      value = value ++ Json.obj("hydra:next" -> routeBuilder(offset + size))
    value
  }

  def advancedSearchPaging(exists: String, concept: String): JsValue =
    searchPagingBuilder(advancedSearchUrl(exists)(concept))

  def searchPaging(exists: String, concept: String): JsValue =
    searchPagingBuilder(searchUrl(exists)(concept))

  def getIds: JsValue = searchPagingBuilder(basketUrl)

  private val advancedSearchUrl: String => String => Long => String = exists =>
    concept =>
      offset =>
        baseUrl + controllers.routes.AdvancedSearchController.handleAdvancedSearchRequest(
          concept,
          q,
          exists,
          format.toString,
          offset,
          if (size <= 1) 10 else size
        )

  private val searchUrl: String => String => Long => String = exists =>
    concept =>
      offset =>
        baseUrl + controllers.routes.SearchController.handleSimpleSearchRequest(
          concept = concept,
          q = q,
          exists = exists,
          format = format.toString,
          offset = offset,
          if (size <= 1) 10 else size
        )

  private val basketUrl: Long => String = offset =>
    baseUrl + controllers.routes.BasketController.handleBasketListRequest(
      q = q,
      format = format.toString,
      offset = offset,
      if (size <= 1) 10 else size
    )
}
