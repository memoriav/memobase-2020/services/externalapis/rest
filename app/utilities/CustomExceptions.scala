package utilities

final case class RestrictedResourceException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)
