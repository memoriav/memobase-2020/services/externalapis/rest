package utilities

import java.util
import java.util.regex.Pattern
import play.api.{Configuration, Logging}

import scala.collection.mutable
import scala.jdk.CollectionConverters._

class ScriptParamBuilder extends Logging {
  val map: mutable.Map[String, AnyRef] = mutable.Map()
  private val matchAllQuery = Pattern.compile("^$|^\\*:\\*$|^\\*$")

  def setBlacklistedDocuments(
      concept: String,
      documents: Map[String, Seq[String]],
      showPublishedDocumentsOnly: Boolean,
      hideInventoryInstitutions: Boolean,
      showPublishedInventoryInstitutionsOnly: Boolean
  ): ScriptParamBuilder = {
    val restricted = new util.ArrayList[
      util.Map[String, util.Map[String, util.List[String]]]
    ]()
    if (showPublishedDocumentsOnly) {
      restricted.add(
        mutable
          .Map(
            "terms" -> mutable
              .Map("isPublished" -> List("false").asJava)
              .asJava
          )
          .asJava
      )
    }
    if (concept == "institution" && showPublishedInventoryInstitutionsOnly) {
      // TODO: At the moment, the flag in the index is inverse. Change the following line after fix applied in index
      restricted.add(
        mutable.Map("terms" -> mutable.Map("isOrWasHolderOf.publicationPermitted" -> List("true").asJava).asJava).asJava
      )
    }
    if (concept == "institution" && hideInventoryInstitutions) {
      restricted.add(
        mutable
          .Map(
            "terms" -> mutable
              .Map(
                "type.keyword" -> List(
                  "memobaseInstitutionInventoryProject"
                ).asJava
              )
              .asJava
          )
          .asJava
      )
    }
    if (documents.nonEmpty) {
      val recordSets: String => util.List[String] = prefix =>
        documents
          .getOrElse("recordSets", List())
          .map(r => s"$prefix$r")
          .asJava
      val institutions: String => util.List[String] = prefix =>
        documents
          .getOrElse("institutions", List())
          .map(i => s"$prefix$i")
          .asJava
      concept match {
        case "record" =>
          val records: util.List[String] = documents
            .getOrElse("records", List())
            .map(r => s"https://memobase.ch/record/$r")
            .asJava
          restricted.add(
            mutable
              .Map("terms" -> mutable.Map("_id" -> records).asJava)
              .asJava
          )
          restricted.add(
            mutable
              .Map(
                "terms" -> mutable
                  .Map("isOrWasPartOf.keyword" -> recordSets("mbrs:"))
                  .asJava
              )
              .asJava
          )
          restricted.add(
            mutable
              .Map(
                "terms" -> mutable
                  // in documents index, this field has the subfield @id
                  .Map("hasOrHadHolder.@id" -> institutions("mbcb:"))
                  .asJava
              )
              .asJava
          )
        case "recordSet" =>
          restricted.add(
            mutable
              .Map(
                "terms" -> mutable
                  .Map(
                    "_id" -> recordSets(
                      // in record-sets index, the id is only the short id
                      ""
                    )
                  )
                  .asJava
              )
              .asJava
          )
          restricted.add(
            mutable
              .Map(
                "terms" -> mutable
                  // in record-sets index, this field doesn't have the subfield @id and values miss the mbcb: prefix
                  .Map("hasOrHadHolder" -> institutions(""))
                  .asJava
              )
              .asJava
          )
        case "institution" =>
          restricted.add(
            mutable
              .Map(
                "terms" -> mutable
                  .Map(
                    "_id" -> institutions(
                      "https://memobase.ch/institution/"
                    )
                  )
                  .asJava
              )
              .asJava
          )
      }
    }
    map += ("restricted" -> (if (restricted.isEmpty)
                               new util.ArrayList[String]()
                             else restricted))
    this
  }

  def setIgnoredFields(ignoredFields: Seq[String]): ScriptParamBuilder = {
    map += "ignoredFields" -> ignoredFields.asJava
    this
  }

  def setSize(
      configuration: Configuration,
      sizeParam: Int
  ): ScriptParamBuilder = {
    val size =
      if (sizeParam > 0 && sizeParam <= configuration.get[Int]("index.maxsize"))
        sizeParam
      else configuration.get[Int]("index.defaultsize")
    map += "size" -> size.asInstanceOf[AnyRef]
    this
  }

  def setOffset(offset: Long): ScriptParamBuilder = {
    map += "from" -> offset.asInstanceOf[AnyRef]
    this
  }

  def setSimpleParamQuery(q: String): ScriptParamBuilder = {
    if (!matchAllQuery.matcher(q).find()) {
      map += ("param_query" -> q)
    }
    this
  }

  def setTermsFilter(terms: Map[String, List[String]]): ScriptParamBuilder = {
    // terms.map(e => Map(e._1 -> e._2.asJava) .foldRight(List[util.Map[String, util.List]])((x, agg) => agg +: Map)
    val list = new util.ArrayList[
      util.Map[String, util.Map[String, util.List[String]]]
    ]()
    for (term <- terms) {
      val l = mutable.ListBuffer[String]()
      l.addAll(term._2)
      val entry: util.Map[String, util.List[String]] = Map(
        term._1 -> l.asJava
      ).asJava
      val newTerm: util.Map[String, util.Map[String, util.List[String]]] = Map(
        "terms" -> entry
      ).asJava
      list.add(newTerm)
    }
    map += ("terms" -> list)
    this
  }

  def setExistsFilterQuery(q: String, exists: String): ScriptParamBuilder = {
    val list = new util.ArrayList[util.Map[String, util.Map[String, String]]]()
    if (matchAllQuery.matcher(q).find()) {
      list.add(
        mutable.Map("match_all" -> mutable.Map[String, String]().asJava).asJava
      )
    } else {
      list.add(
        mutable.Map("multi_match" -> mutable.Map("query" -> q).asJava).asJava
      )
    }
    if (exists.nonEmpty) {
      val properties = exists.split(',')
      // TODO: Add a check if a property even exists?
      for (property <- properties) {
        list.add(
          mutable
            .Map("exists" -> mutable.Map("field" -> property).asJava)
            .asJava
        )
      }
      map += ("filters" -> list)
    }
    this
  }

}
