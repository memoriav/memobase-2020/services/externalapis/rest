package utilities

import co.elastic.clients.elasticsearch._types.mapping.Property
import co.elastic.clients.elasticsearch.indices.GetMappingRequest
import modules.ElasticsearchComponent
import play.Logger
import play.api.Configuration
import play.api.libs.json.{JsArray, JsObject, JsString, JsValue, Json}

import scala.collection.mutable
import scala.jdk.CollectionConverters._
import scala.util.{Failure, Success, Try}

object PropertyHelper {
  val logger: Logger.ALogger = Logger.of(getClass)

  def getPropertyMap(
      concept: ConceptType,
      configuration: Configuration,
      es: ElasticsearchComponent
  ): Try[JsValue] = {
    getPropertiesMap(configuration.get[String](s"concept2index.$concept"), es)
      .map(source =>
        source.sortBy {
          case s: JsString => s.value
          case o: JsObject => o.value.head._1
          case v: JsValue  => v.toString
        }
      )
      .map(seq => Json.toJson(seq))
  }

  private def getPropertiesMap(
      index: String,
      es: ElasticsearchComponent
  ): Try[Seq[JsValue]] = {
    logger.debug(s"Getting mapping properties for index $index")
    val iterator = Try {
      es.client.indices.getMapping((req: GetMappingRequest.Builder) =>
        req.index(index)
      )
    } match {
      case Success(result) => result.result.values.iterator
      case Failure(ex)     => return Failure(ex)
    }
    Try {
      iterator.next
    } match {
      case Success(nextElem) => scanPropertiesMap(nextElem.mappings.properties)
      case Failure(ex)       => Failure(ex)
    }
  }

  private def scanPropertiesMap(
      map: java.util.Map[String, Property]
  ): Try[Seq[JsValue]] = {
    val res = map.asScala
      .map(o => {
        if (o._2.isObject) {
          scanPropertiesMap(
            o._2.`object`.properties
          ) match {
            case Success(properties) =>
              JsObject(Map(o._1 -> JsArray(properties)))
            case Failure(ex) => return Failure(ex)
          }
        } else {
          JsString(o._1)
        }
      })
      .foldRight(Seq[JsValue]())((x, agg) => {
        agg :+ x
      })
    Success(res)
  }

}
