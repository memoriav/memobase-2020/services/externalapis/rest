package utilities

object EMailObfuscator {
  def obfuscate(eMailAddress: String): String =
    eMailAddress.map(_.charValue match {
      case i if i >= 8364 => 129.toChar
      case i              => (i + 1).toChar
    })

  def deobfuscate(eMailAddress: String): String =
    eMailAddress.map(_.charValue match {
      case i if i >= 8364 => 127.toChar
      case i              => (i - 1).toChar
    })
}
