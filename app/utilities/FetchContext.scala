package utilities

sealed trait FetchContext

case object Scroll extends FetchContext

case class From(offset: Long) extends FetchContext
