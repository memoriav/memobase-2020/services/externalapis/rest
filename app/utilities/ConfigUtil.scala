package utilities

import play.api.Configuration

import scala.util.Try

object ConfigUtil {

  val showPublishedDocumentsOnly: Configuration => Boolean =
    (c: Configuration) =>
      c.has("showPublishedDocumentsOnly") &&
        Try(c.get[String]("showPublishedDocumentsOnly").toBoolean)
          .getOrElse(true)

  val hideInventoryInstitutions: Configuration => Boolean =
    (c: Configuration) =>
      c.has("hideInventoryInstitutions") &&
        Try(c.get[String]("hideInventoryInstitutions").toBoolean)
          .getOrElse(false)

  val showPublishedInventoryInstitutionsOnly: Configuration => Boolean =
    (c: Configuration) =>
      c.has("showPublishedInventoryInstitutionsOnly") &&
        Try(c.get[String]("showPublishedInventoryInstitutionsOnly").toBoolean)
          .getOrElse(true)
}
