package utilities

import scala.language.implicitConversions

sealed trait ConceptType {
  protected val name: String
  override def toString: String = name
}

object ConceptType {
  implicit def getName(x: ConceptType): String = x.name
  implicit def withName(x: String): ConceptType = x match {
    case Record.name      => Record
    case RecordSet.name   => RecordSet
    case Institution.name => Institution
    case _                => UnknownConcept
  }
}

case object Record extends ConceptType {
  override protected[utilities] val name: String = "record"
}

case object RecordSet extends ConceptType {
  override protected[utilities] val name: String = "recordSet"
}

case object Institution extends ConceptType {
  override protected[utilities] val name: String = "institution"
}

case object UnknownConcept extends ConceptType {
  override protected[utilities] val name: String = "unknown"
}
