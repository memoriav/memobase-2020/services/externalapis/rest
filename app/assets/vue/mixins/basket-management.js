export default {
    methods: {
        updateSessionStorage: function (sig) {
            const id = "m_" + sig
            const checkedItems = sessionStorage.getItem("checkedItems")
            if (sessionStorage.getItem(id) === null) {
                sessionStorage.setItem(id, "")
                if (checkedItems === null) {
                    sessionStorage.setItem("checkedItems", sig)
                } else {
                    sessionStorage.setItem("checkedItems", checkedItems + "&" + sig)
                }
            } else {
                sessionStorage.removeItem(id)
                if (checkedItems === sig) {
                    sessionStorage.removeItem("checkedItems")
                } else if (checkedItems.startsWith(sig)) {
                    sessionStorage.setItem("checkedItems", checkedItems.substring(sig.length + 1))
                } else {
                    sessionStorage.setItem("checkedItems", checkedItems.replace("&" + sig, ""))
                }
            }
            return sessionStorage.getItem("checkedItems")
        },
        addOrIgnoreItemInSessionStorage: function (sig) {
            const id = "m_" + sig
            if (sessionStorage.getItem(id) === null) {
                sessionStorage.setItem(id, "")
            }
            const checkedItems = sessionStorage.getItem("checkedItems")
            if (!checkedItems) {
                sessionStorage.setItem("checkedItems", sig)
            } else if (!checkedItems.includes(sig)) {
                sessionStorage.setItem("checkedItems", checkedItems + "&" + sig)
            }
        },
        recordInBasket: function (sig) {
            return sessionStorage.getItem(`m_${sig}`) !== null
        },
        initialiseSessionStorage: function (encodedString) {
            const decodedString = LZString.decompressFromEncodedURIComponent(encodedString)
            const sigs = decodedString.split("&")
            for (const sig of sigs) {
                this.addOrIgnoreItemInSessionStorage(sig)
            }
            return sigs
        }
    }
}