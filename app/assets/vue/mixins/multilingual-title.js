export default {
    methods: {
        generateMultilingualTitle: function (val, titleFieldPrefix) {
            const titleDe = `${titleFieldPrefix}De`
            const titleFr = `${titleFieldPrefix}Fr`
            const titleIt = `${titleFieldPrefix}It`
            if (val[titleDe] === val[titleFr]) {
                if (val[titleDe] === val[titleIt]) {
                    return val[titleDe]
                } else {
                    return `${val[titleDe]} - ${val[titleIt]}`
                }
            } else {
                if (val[titleIt] !== val[titleDe] || val[titleIt] !== val[titleFr]) {
                    return `${val[titleDe]} - ${val[titleFr]} - ${val[titleIt]}`
                } else if (val[titleIt] !== val[titleDe]) {
                    return `${val[titleDe]} - ${val[titleIt]}`
                } else {
                    return `${val[titleFr]} - ${val[titleIt]}`
                }
            }
        }
    }
};