package libraries

import play.api.http.MediaRange

object Accept {

  private def getFormat(
      format: String,
      acceptHeaders: Seq[MediaRange]
  ): Option[Format] = {
    checkFormat(format).orElse(checkAcceptHeaders(acceptHeaders))
  }

  def formatOrHtml(format: String, acceptHeaders: Seq[MediaRange]): Format =
    getFormat(format, acceptHeaders).getOrElse(Html)

  private def checkAcceptHeaders(
      acceptHeaders: Seq[MediaRange]
  ): Option[Format] = {
    val acceptHeadersSet = acceptHeaders.map(_.toString()).toSet
    Format.list.find(v => (v.mimeTypes intersect acceptHeadersSet).nonEmpty)
  }

  private def checkFormat(format: String): Option[Format] =
    Format.list.find(_.name == format)
}
