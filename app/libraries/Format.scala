package libraries

sealed trait Format {
  val name: String
  val mimeTypes: Set[String]
}
case object Html extends Format {
  override val name = "html"
  override val mimeTypes = Set("text/html")
}
case object JsonLd extends Format {
  override val name = "json"
  override val mimeTypes = Set("application/json", "application/ld+json")
}
case object JsonLines extends Format {
  override val name = "jsonl"
  override val mimeTypes = Set("application/x-jsonlines")
}

object Format {
  val list = List(Html, JsonLd, JsonLines)
}
