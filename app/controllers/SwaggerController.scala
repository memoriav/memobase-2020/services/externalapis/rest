package controllers

import play.api.mvc.{
  AbstractController,
  Action,
  AnyContent,
  ControllerComponents
}

import javax.inject._

/** This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */
//noinspection ScalaUnusedSymbol
class SwaggerController @Inject() (cc: ControllerComponents)
    extends AbstractController(cc) {
  def handleSwaggerViewRequest: Action[AnyContent] = Action { implicit request =>
    Ok(views.html.swagger())
  }
}
