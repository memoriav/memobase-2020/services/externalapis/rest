package controllers

import io.swagger.annotations.{Api, ApiParam, ApiResponse, ApiResponses}
import libraries.{Accept, Html, JsonLd, JsonLines}
import modules.{ElasticsearchComponent, ElasticsearchProperties}
import play.api.{Configuration, Logging}
import play.api.mvc.{
  AbstractController,
  Action,
  AnyContent,
  ControllerComponents,
  Rendering
}
import utilities.{ResultScroller, SearchHelper}

import javax.inject.{Inject, Singleton}

@Singleton
@Api(value = "Basket endpoint")
@ApiResponses(
  Array(
    new ApiResponse(code = 200, message = "Basket request successful"),
    new ApiResponse(code = 400, message = "Invalid search parameters")
  )
)
class BasketController @Inject() (
    cc: ControllerComponents,
    config: Configuration,
    es: ElasticsearchComponent,
    esProp: ElasticsearchProperties
) extends AbstractController(cc)
    with Rendering
    with Logging {
  val searchHelper = new SearchHelper(es, config)

  val limit: Int => Int = x => if (x < 1) 10 else if (x > 100) 100 else x

  def handleBasketListRequest(
      @ApiParam(value = "Compressed document ids") q: String,
      @ApiParam(
        value = "Response format. `json` (default) and `jsonl` (jsonlines)"
      ) format: String,
      @ApiParam(value = "Result set offset") offset: Long,
      @ApiParam(
        value = "Size of result set.\n" +
          "  - `json` response: Defaults to `10`, must be between `1` and `100`\n" +
          "  - `jsonl` response: Defaults to `0` (i.e. unlimited)"
      ) size: Int
  ): Action[AnyContent] = Action { implicit request =>
    try {
      val responseFormat = Accept.formatOrHtml(format, request.acceptedTypes)
      responseFormat match {
        case Html =>
          val response =
            searchHelper.searchByIds(
              q,
              responseFormat,
              offset,
              limit(size)
            )
          Ok(
            views.html
              .results(response, "record", q, esProp.esProperties("record"))
          )
        case JsonLd =>
          val response =
            searchHelper.searchByIds(
              q,
              responseFormat,
              offset,
              limit(size)
            )
          Ok(response.toString).as("application/ld+json")
        case JsonLines =>
          ResultScroller.scrollThroughBasket(q, es, config, size)
      }
    } catch {
      case e: Throwable => BadRequest(views.html.error(e.getMessage, "record"))
    }

  }
}
