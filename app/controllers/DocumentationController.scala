package controllers

import modules.ElasticsearchProperties
import play.api.Configuration
import play.api.mvc.{
  AbstractController,
  Action,
  AnyContent,
  ControllerComponents
}

import javax.inject._

/** This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */
//noinspection ScalaUnusedSymbol
class DocumentationController @Inject() (
    cc: ControllerComponents,
    config: Configuration,
    esProp: ElasticsearchProperties
) extends AbstractController(cc) {
  def handleDocumentationRequest(concept: String): Action[AnyContent] = Action {
    implicit request =>
      Ok(views.html.documentation(concept, esProp.esProperties(concept)))
  }
}
