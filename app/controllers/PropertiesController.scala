package controllers

import io.swagger.annotations.{Api, ApiParam, ApiResponse}
import modules.ElasticsearchComponent
import play.api.Configuration
import play.api.mvc._
import utilities._

import javax.inject._
import scala.util.Success
import scala.util.Failure

/** This controller creates an `Action` to handle HTTP requests for the property
  * map
  */
@Singleton
@Api(value = "Show properties")
@ApiResponse(code = 200, message = "Properties successfully retrieved")
class PropertiesController @Inject() (
    cc: ControllerComponents,
    config: Configuration,
    es: ElasticsearchComponent
) extends AbstractController(cc)
    with Rendering {
  val searchHelper = new SearchHelper(es, config)

  def handlePropertiesRequest(
      @ApiParam(
        value = "Concept type. `record`, `recordSet` or `institution`"
      ) concept: String
  ): Action[AnyContent] = Action { implicit request =>
    PropertyHelper.getPropertyMap(concept, config, es) match {
      case Success(res) => Ok(res.toString).as("application/json")
      case Failure(ex)  => InternalServerError
    }
  }
}
