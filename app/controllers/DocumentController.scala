package controllers

import io.swagger.annotations.{Api, ApiParam, ApiResponse, ApiResponses}
import libraries._
import modules.{ElasticsearchComponent, ElasticsearchProperties}
import play.api.Configuration
import play.api.mvc._
import utilities._

import javax.inject._

/** This controller creates an `Action` to handle requests on individual
  * documents
  */
@Singleton
@Api(value = "Individual document endpoint")
@ApiResponses(
  Array(
    new ApiResponse(code = 200, message = "Document successfully retrieved"),
    new ApiResponse(code = 400, message = "Invalid parameter")
  )
)
class DocumentController @Inject() (
    cc: ControllerComponents,
    config: Configuration,
    es: ElasticsearchComponent,
    esProp: ElasticsearchProperties
) extends AbstractController(cc)
    with Rendering {
  val searchHelper = new SearchHelper(es, config)

  def handleSingleDocumentRequest(
      @ApiParam(value = "Document's id") id: String,
      format: String,
      @ApiParam(
        value = "Concept type. `record`, `recordSet` or `institution`"
      ) concept: String
  ): Action[AnyContent] = Action { implicit request =>
    val responseFormat = Accept.formatOrHtml(format, request.acceptedTypes)
    val fieldExclusionTable: Map[String, Map[String, Seq[String]]] = {
      List("records", "recordSets", "institutions")
        .filter(entity => config.has(s"ignoredFields.$entity"))
        .foldRight(Map[String, Map[String, Seq[String]]]()) {
          (entity, aggEntity) =>
            aggEntity + (entity -> {
              config
                .get[Map[String, Seq[String]]](s"ignoredFields.$entity")
                .foldRight(Map[String, Seq[String]]()) { (idList, agg1) =>
                  {
                    agg1 ++ idList._1
                      .split(",")
                      .foldRight(Map[String, Seq[String]]())((id, agg2) =>
                        agg2 + (id -> idList._2)
                      )
                  }
                }
            })
        }
    }
    try {
      val response = searchHelper.searchById(id, concept)
      responseFormat match {
        case Html =>
          val (recordSetId, institutionId) =
            searchHelper.getRecordSetAndInstitutionIdFromResult(response)
          val recordSetName =
            recordSetId.flatMap(x => Some(searchHelper.getRecordSetNameById(x)))
          val institutionName = institutionId.flatMap(x =>
            Some(searchHelper.getInstitutionNameById(x))
          )
          Ok(
            views.html.document(
              JsonUtil
                .filterAndSortJson(
                  response,
                  id,
                  fieldExclusionTable.getOrElse(concept + "s", Map())
                )
                .toString,
              concept,
              recordSetName.getOrElse(""),
              institutionName.getOrElse(""),
              esProp.esProperties(concept),
              JsonUtil.hideUiElements(
                id,
                fieldExclusionTable.getOrElse(concept + "s", Map())
              )
            )
          )
        case JsonLd =>
          Ok(
            JsonUtil
              .filterAndSortJson(
                response,
                id,
                fieldExclusionTable.getOrElse(concept + "s", Map())
              )
              .toString
          )
            .as(libraries.JsonLd.mimeTypes.head)
        case JsonLines =>
          Ok(
            JsonUtil
              .filterAndSortJson(
                response,
                id,
                fieldExclusionTable.getOrElse(concept + "s", Map())
              )
              .toString
          )
            .as(libraries.JsonLines.mimeTypes.head)
      }
    } catch {
      case e: RestrictedResourceException =>
        Forbidden(views.html.error(e.getMessage, concept))
      case e: Throwable => NotFound(views.html.error(e.getMessage, concept))
    }
  }
}
