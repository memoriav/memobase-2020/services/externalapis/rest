package controllers

import co.elastic.clients.elasticsearch.core.search.Hit
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import io.swagger.annotations.{Api, ApiParam, ApiResponse, ApiResponses}
import libraries._
import modules.{ElasticsearchComponent, ElasticsearchProperties}
import play.api.Configuration
import play.api.libs.json.jackson.PlayJsonMapperModule
import play.api.libs.json.{JsArray, JsObject, JsValue, Json, JsonParserSettings}
import play.api.mvc._
import utilities._

import java.util.stream.Collectors
import javax.inject._
import scala.jdk.javaapi.CollectionConverters

/** This controller creates an `Action` to handle HTTP requests on the advanced
  * search endpoint
  */
@Singleton
@Api(value = "Advanced search endpoint")
@ApiResponses(
  Array(
    new ApiResponse(code = 200, message = "Search successful"),
    new ApiResponse(code = 400, message = "Invalid search parameters")
  )
)
class AdvancedSearchController @Inject() (
    cc: ControllerComponents,
    config: Configuration,
    es: ElasticsearchComponent,
    esProp: ElasticsearchProperties
) extends AbstractController(cc)
    with Rendering {
  val searchHelper = new SearchHelper(es, config)

  val limit: Int => Int = x => if (x < 1) 10 else if (x > 100) 100 else x

  val mapper: ObjectMapper =
    new ObjectMapper().registerModule(
      new PlayJsonMapperModule(JsonParserSettings())
    )

  def handleAdvancedSearchRequest(
      @ApiParam(
        value = "Concept type. `record`, `recordSet` or `institution`"
      ) concept: String,
      @ApiParam(value = "Query string") q: String,
      @ApiParam(
        value = "Comma-separated list of fields which must exist in documents"
      ) exists: String,
      @ApiParam(
        value = "Response format. `json` (default) or `jsonl` (jsonlines)."
      ) format: String,
      @ApiParam(value = "Result set offset") offset: Long = 0,
      @ApiParam(
        value = "Size of result set.\n" +
          "  - `json` response: Defaults to `10`, must be between `1` and `100`\n" +
          "  - `jsonl` response: Defaults to `0` (i.e. unlimited)"
      ) size: Int
  ): Action[AnyContent] = Action { implicit request =>
    val responseFormat = Accept.formatOrHtml(format, request.acceptedTypes)
    try {
      val advancedSearchHelper = new AdvancedSearchHelper(
        q,
        offset,
        if (size < 1) 10 else size,
        ConceptType.withName(concept),
        config,
        es.client
      )
      responseFormat match {
        case Html =>
          val hydraCollection = advancedSearchWithPaging(
            advancedSearchHelper,
            q,
            exists,
            responseFormat,
            offset,
            limit(size),
            concept
          )
          Ok(
            views.html.results(
              hydraCollection,
              concept,
              "",
              esProp.esProperties(concept)
            )
          )
        case JsonLd =>
          val hydraCollection = advancedSearchWithPaging(
            advancedSearchHelper,
            q,
            exists,
            responseFormat,
            offset,
            limit(size),
            concept
          )
          Ok(hydraCollection).as("application/ld+json")
        case JsonLines =>
          val response = advancedSearchHelper.execute(true)
          ResultScroller.scrollThroughAdvancedSearchResultList(
            response,
            es,
            config,
            size
          )
      }
    } catch {
      case e: Throwable => BadRequest(views.html.error(e.getMessage, concept))
    }
  }

  private def advancedSearchWithPaging(
      advancedSearchHelper: AdvancedSearchHelper,
      q: String,
      exists: String,
      responseFormat: Format,
      offset: Long,
      size: Int,
      concept: String
  ): JsValue = {
    val response = advancedSearchHelper.execute()
    val pagingHelper = new PagingMetadataHelper(
      q,
      responseFormat,
      offset,
      size,
      response.hits.total.value,
      config
    )
    val hydraPartialCollectionView =
      pagingHelper.advancedSearchPaging(exists, concept)
    val hits = CollectionConverters
      .asScala(response.hits.hits.iterator)
      .map(v => mapper.treeToValue(v.source, classOf[JsObject]))
      .toIndexedSeq
    Json.obj(
      "@id" -> hydraPartialCollectionView("@id")
        .as[String]
        .replaceAll("&?offset=[^&^]+", "")
        .replaceAll("&?size=[^&^]+", ""),
      "@type" -> "hydra:Collection",
      "@context" -> Json.obj(
        "hydra" -> "http://www.w3.org/ns/hydra/core#"
      ),
      "hydra:member" -> new JsArray(hits),
      "hydra:totalItems" -> response.hits.total.value,
      "hydra:view" -> hydraPartialCollectionView
    )
  }
}
