package controllers

import io.swagger.annotations.{Api, ApiParam, ApiResponse, ApiResponses}

import javax.inject._
import libraries.Accept
import modules.{ElasticsearchComponent, ElasticsearchProperties}
import play.api.Configuration
import play.api.mvc._
import utilities._
import libraries._

/** This controller creates an `Action` to handle HTTP requests on the search
  * endpoint
  */
@Singleton
@Api(value = "Simple search endpoint")
@ApiResponses(
  Array(
    new ApiResponse(code = 200, message = "Search successful"),
    new ApiResponse(code = 400, message = "Invalid search parameters")
  )
)
class SearchController @Inject() (
    cc: ControllerComponents,
    config: Configuration,
    es: ElasticsearchComponent,
    esProp: ElasticsearchProperties
) extends AbstractController(cc)
    with Rendering {
  val searchHelper = new SearchHelper(es, config)

  val limit: Int => Int = x => if (x < 1) 10 else if (x > 100) 100 else x

  def handleSimpleSearchRequest(
      @ApiParam(
        value = "Concept type. One of `record`, `recordSet`, `institution`"
      ) concept: String,
      @ApiParam(value = "Query string") q: String,
      @ApiParam(
        value = "Comma-separated list of fields which must exist in documents"
      ) exists: String,
      @ApiParam(
        value = "Response format. `json` (default) or `jsonl` (jsonlines)"
      ) format: String,
      @ApiParam(value = "Result set offset") offset: Long,
      @ApiParam(
        value = "Size of result set.\n" +
          "  - `json` response: Defaults to `10`, must be between `1` and `100`\n" +
          "  - `jsonl` response: Defaults to `0` (i.e. unlimited)"
      ) size: Int
  ): Action[AnyContent] = Action { implicit request =>
    val responseFormat = Accept.formatOrHtml(format, request.acceptedTypes)
    try {
      responseFormat match {
        case Html =>
          val response = searchHelper.search(
            q,
            exists,
            responseFormat,
            offset,
            limit(size),
            concept
          )
          Ok(
            views.html
              .results(response, concept, q, esProp.esProperties(concept))
          )
        case JsonLd =>
          val response = searchHelper.search(
            q,
            exists,
            responseFormat,
            offset,
            limit(size),
            concept
          )
          Ok(response.toString).as("application/ld+json")
        case JsonLines =>
          ResultScroller.scrollThroughResultList(q, exists, es, config, size)
      }
    } catch {
      case e: Throwable => BadRequest(views.html.error(e.getMessage, concept))
    }
  }
}
