function DeobfuscateEMail(e) {
    let s = e.getAttribute("href");
    let n = 0;
    let r = "";
    for (let i = 0; i < s.length; i++) {
        n = s.charCodeAt(i);
        if (n >= 8364) {
            n = 128;
        }
        r += String.fromCharCode(n - 1);
    }
    e.setAttribute("href", r);
}