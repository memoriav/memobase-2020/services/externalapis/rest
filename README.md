# REST API for memobase.ch

<https://api.memobase.ch> (_not yet functional_) provides a Rest API to the content of <https://memobase.ch> in a Linked
Open Data Format.

See the [documentation](https://api.memobase.ch/documentation).

The application uses [Play Framework (Scala)](https://www.playframework.com/) and [Vue.js](https://vuejs.org) for
rendering (via this [sbt plugin](https://github.com/GIVESocialMovement/sbt-vuefy)).

## Configuration

The application looks in `conf/application.conf` for general settings. `application.conf` contains all settings which
you normally can leave as are, but there are some which you have to adapt to your needs. Namely

- `baseUrl`: The application's base url.
- `concept2index.record`: The name of the index containing the records' metadata.
- `concept2index.recordSet`: The name of the index containing the record sets' metadata.
- `concept2index.institution`: The name of the index containing the institutions' metadata.
- `index.hosts`: List of Elasticsearch hosts (`<host:port>`).
- `index.cluster`: Elasticsearch cluster name.

You have two options to override or append settings:

- Add an additional file named `development.conf`, `test.conf` or `production.conf` providing the actual values. Use
  the `+=` operator to append instead of overwrite a value.
  See [`conf/development.conf.dist`](conf/development.conf.dist) for an example.
- Some values can (or must if you don't use an additional file) be set via environment variables. These values are
    - `BASE_URL`: The application's base url
    - `ELASTIC_HOST_LIST`: Elasticsearch host list
    - `ELASTIC_CLUSTER_NAME`: Elasticsearch cluster name
    - `ELASTIC_API_KEY_ID`: Id part of the API key used to authenticate with Elasticsearch
    - `ELASTIC_API_KEY_SECRET`: Secret part of the API key used to authenticate with Elasticsearch
    - `CA_CERT_PATH`: Path to the CA certificate used to verify Elasticsearch's SSL certificate
    - `RECORD_INDEX`: Records' metadata index name
    - `RECORD_SET_INDEX`: Record sets' metadata index name
    - `INSTITUTION_INDEX`: Institutions' metadata index name

### Blacklisting documents 

Records, record sets and institutions can selectively be hidden.

The RestAPI allow to control if and what documents are hidden in the UI and its derivatives (JSON documents).

Use the __`blacklist`__ property in the application settings to hide documents based on the id. `blacklist` expects an
object with one or several of the following keys: `records`, `recordSets` and `institutions`. As the names imply, each
key references by id a list of records, recordSets or institutions which should be blacklisted. E.g.:

```conf
blacklist = {
    records = ["srf-005-C3E8203C-1353-4530-98D8-7FBE2B6F45CB_02"]
    institutions = ["lkb", "afz"]
}
```

Furthermore, you can hide documents based on certain attributes:

- Documents which are not published on Memobase are hidden by
  default. To override this, set `showPublishedDocumentsOnly` to `false`.
- To _disable_ "inventory institutions" , set `hideInventoryInstitutions` to
  `true`.
- To _enable_ hidden "inventory institutions", set `showPublishedInventoryInstitutionsOnly` to `false`.


### Disable fields

The __`ignoredFields`__ comprises the same fields - `records`, `recordSets` and `institutions` as the __`blacklist`__
property and allows for hiding
individual fields. It is however a bit more complex than the `blacklist` property:

- Each field is itself an object with list values, where a key can be
  - a single document, recordSet or institution like `baz-001-xyz1023`,
  - a comma-separated list of documents, recordSets or institutions like `aag-001,abe-001` or
  - an underscore (`_`) meaning all enitity instances
- The values are literal names of fields. There are two special cases:
  - `_thumbnail` hides the thumbnail in the record view.
  - As the field names defined in the `_` are filtered on the Elasticsearch index level, you can use wildcard syntax
    supported by Elasticsearch.


## Production build

The productive build uses the sbt native packager to create an executable build. Thanks to sbt-vuefy, the vue.js code is
compiled in productive mode as well.

```shell 
sbt stage
```

## Development setup

0. Install [`sbt`](https://scala-sbt.org) and [`yarn`](https://yarnpkg.com).

1. Adapt configuration to your needs. [See above](#configuration) for hints.

2. Install Vue.js dependencies:

```shell
yarn install
```

3. Start a local Elasticsearch instance

```shell
docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" index.docker.io/elasticsearch:7.5.1
```

4. Load test data into indices

```shell
cd dev/
./load-data.sh
```

5. Run application and open application in browser (default: [http://localhost:9000])

```shell
BASE_URL=localhost:9000 sbt run
```
