package modules

import org.scalatest.funsuite.AnyFunSuiteLike

class CustomElasticsearchClientTest extends AnyFunSuiteLike {

  test("testParseHost") {
    val hosts =
      "https://example.com:1324,https://example2.com:1234,https://example3.com:1234"
    assert(CustomElasticsearchClient.parseHost(hosts).isSuccess)

  }

}
