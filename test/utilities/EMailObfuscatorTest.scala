package utilities

import org.scalatest.flatspec.AnyFlatSpec

class EMailObfuscatorTest extends AnyFlatSpec {

  "Given a valid e-mail address" should "result in the same string after obfuscating and deobfuscating" in {
    val eMailAddress = "test.17@example.com"
    val obfuscated = EMailObfuscator.obfuscate(eMailAddress)
    val deobfuscated = EMailObfuscator.deobfuscate(obfuscated)
    assert(eMailAddress == deobfuscated)
  }

}
