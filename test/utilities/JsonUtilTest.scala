package utilities

import org.scalatest.flatspec.AnyFlatSpec
import play.api.libs.json.Json
import play.api.libs.json.JsValue

//noinspection NameBooleanParameters
class JsonUtilTest extends AnyFlatSpec {
  val jsonObj = """{
    "@context": "https://api.memobase.ch/context.json",
    "@id": "mbr:aag-001-RBA1-4-15453_1",
    "@type": "rico:Record",
    "scopeAndContent": "Quelle: RiBiDi",
    "created": {
        "@type": "rico:SingleDate",
        "normalizedDateValue": "1966-03-17"
    },
    "hasInstantiation": [
        {
            "isOrWasRegulatedBy": [
                { "name": "Copyright Not Evaluated (CNE)"}
            ]
        }
    ],
    "title": "John Harlin am Grabe von Hilti von Allmen"
}"""

  "Given a JSON document and a filter map" should "result in a correctly filtered JSON object" in {

    val expected =
      """{"@context":"https://api.memobase.ch/context.json","@id":"mbr:aag-001-RBA1-4-15453_1","@type":"rico:Record","created":{"@type":"rico:SingleDate"},"hasInstantiation":[{"isOrWasRegulatedBy":[{}]}],"title":"John Harlin am Grabe von Hilti von Allmen"}"""
    val json: JsValue = Json.parse(jsonObj)
    val exclusionRules =
      Map(
        "_" -> List("some", "other"),
        "aag,xyz-001" -> List(
          "scopeAndContent",
          "created.normalizedDateValue",
          "hasInstantiation.isOrWasRegulatedBy.name"
        ),
        "bag" -> List("title")
      )
    val result =
      JsonUtil
        .filterAndSortJson(json, "aag-001-abcdef", exclusionRules)
        .toString
    assert(result == expected)
  }
}
