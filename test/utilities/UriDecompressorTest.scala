package utilities

import org.scalatest.flatspec.AnyFlatSpec

//noinspection NameBooleanParameters
class UriDecompressorTest extends AnyFlatSpec {

  "Given a compressed string" should "result in a correctly uncompressed string" in {
    val compressedString =
      "EYNwxgtADFCMGwOxIGywGRgCYBdpQCYIBzAQwCdoAORAfVgNoAcg"
    val expectedResult = "bvc-001-171761&cdt-002-gar-087_12_p"
    val actualResult = UriDecompressor.decompress(compressedString)
    assert(actualResult == expectedResult)
  }
}
