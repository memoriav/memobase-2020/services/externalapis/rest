import sbt._

object Dependencies {
  lazy val jacksonV = "2.17.+"
  lazy val scalaTestPlusPlugin =
    "org.scalatestplus.play" %% "scalatestplus-play" % "7.+"
  lazy val esJavaClient =
    "co.elastic.clients" % "elasticsearch-java" % "8.13.+"
  lazy val jacksonDatabind =
    "com.fasterxml.jackson.core" % "jackson-databind" % jacksonV
  lazy val jacksonModuleScala =
    "com.fasterxml.jackson.module" %% "jackson-module-scala" % jacksonV
  lazy val log4jCore = "org.apache.logging.log4j" % "log4j-core" % "2.23.+"
  lazy val swaggerCore = "io.swagger" % "swagger-core" % "1.6.+"
  lazy val swaggerPlay = "com.github.dwickern" %% "swagger-play3.0" % "4.+"
}
