#!/bin/bash

# podman network create es-dev
# podman run -d --name elasticsearch --net es-dev -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" index.docker.io/elasticsearch:7.5.1

ES_PORT=9200
DOCUMENT_INDEX="records"
COLLECTION_INDEX="collections"
INSTITUTION_INDEX="institutions"

INDIZES=("$DOCUMENT_INDEX" "$COLLECTION_INDEX" "$INSTITUTION_INDEX")

for index in "${INDIZES[@]}"; do
  curl -XPUT "localhost:${ES_PORT}/${index}" -H 'Content-Type: application/json' -d '
{
  "settings" : {
    "number_of_shards" : 1,
    "number_of_replicas" : 1,
    "mapping.depth.limit": 50,
    "mapping.total_fields.limit": 2000
  }
}
'
  for f in testdata/${index}/*.json; do
    id=${f##*/}
    id=${id:0:-5}
    curl -H "Content-Type: application/json" -XPUT "http://localhost:${ES_PORT}/${index}/_doc/${id}?pretty" --data-binary @$f
  done
done
