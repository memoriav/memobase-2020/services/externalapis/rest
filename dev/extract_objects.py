#!/usr/bin/python

# Converts dumps from Kafka topic into a form handable by ES. It
# expects the dump in such a way that key and value are separated
# by tab and each message separated by a newline character.
# 
# The program is started inside the testdata/<subfolder> dirs with three arguments: <Path to input
# file> <memobase collection to be extracted> <max number of
# documents to be considered>

import sys
from pyld import jsonld
import json
import jq

institutions_frame = {"@type": "https://www.ica.org/standards/RiC/ontology#CorporateBody"}
collections_frame = {"@type": "https://www.ica.org/standards/RiC/ontology#RecordSet"}
records_frame = {}
with open('../context.json') as f:
    records_frame = json.load(f)

frame = dict()
if 'institutions' in sys.argv[1]:
    frame = institutions_frame
elif 'collections' in sys.argv[1]:
    frame = collections_frame
elif 'records' in sys.argv[1]:
    frame = records_frame

files=0
stop=False

with open(sys.argv[1]) as f:
    for l in f:
        split = l.split("\t")
        if sys.argv[2] in split[0]:
            files += 1
            if files > int(sys.argv[3]):
                stop=True
                break
            id=split[0].split("/")[-1]
            j=json.loads(split[1])
            
            with open(id + ".json", "w") as f2:
                if 'collections' in sys.argv[1]:
                    frame['@id'] = split[0]
                f2.write(json.dumps(jsonld.frame(j, frame=frame)))
        if stop:
            break

