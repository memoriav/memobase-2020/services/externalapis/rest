FROM eclipse-temurin:21-jre-noble
WORKDIR /app
COPY target/app /app/
ENTRYPOINT ./bin/memobase-rest-api
